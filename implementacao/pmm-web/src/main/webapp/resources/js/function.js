function clearField(idField) {
	var inputs = document.getElementsByTagName("input");
	var selects = document.getElementsByTagName("select");
	var labels = document.getElementsByTagName("label");
	/* var textAreas = document.getElementsByTagName("label"); */

	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].id.indexOf(idField) >= 0) {
			inputs[i].value = "";
			break;
		}
	}

	for (var i = 0; i < selects.length; i++) {
		if (selects[i].id.indexOf(idField) >= 0) {
			selects[i].value = "";
			break;
		}
	}

	for (var i = 0; i < labels.length; i++) {
		if (labels[i].id.indexOf(idField) >= 0) {
			labels[i].innerHTML = "...";
			break;
		}
	}
}

function clearFields(elementId) {
	var element = document.getElementById(elementId);
	var inputs = element.getElementsByTagName("input");
	var selects = element.getElementsByTagName("select");
	var labels = document.getElementsByTagName("label");

	/* var textAreas = document.getElementsByTagName("label"); */

	for (var i = 0; i < inputs.length; i++) {

		if (inputs[i].id.indexOf('ipt') >= 0) {
			inputs[i].value = "";
		}
	}

	for (var i = 0; i < selects.length; i++) {

		if (selects[i].id.indexOf('ipt') >= 0) {
			selects[i].value = "";
		}
	}

	for (var i = 0; i < labels.length; i++) {

		if (labels[i].id.indexOf('ipt') >= 0) {
			labels[i].innerHTML = "...";
		}
	}

	closeMessPanelIfOpen();
}

function handleSubmitRequest(xhr, status, args) {
	if(args.validationFailed == null){
		closeMessPanelIfOpen();
	}
}

function closeMessPanelIfOpen() {

	try {

		setTimeout(
				"document.getElementById('templateForm:globalMess').children[0].children[0].click()",
				4000);

	} catch (err) {

	}
}

function setBehaviorForm(operation) {

	if (operation == 1) {
		var labels = document.getElementsByTagName("label");
		for ( var i = 0; i < labels.length; i++) {

			if ((labels[i].id != "")) {
				var labelValue = labels[i].innerHTML;
				var x = labelValue.split("\t");
				labelValue = x[x.length - 1];
				labels[i].innerHTML = labelValue;
				labels[i].style.fontWeight = "normal";
			}
		}
	}

	else if (operation == 2) {

		var labels = document.getElementsByTagName("label");
		for ( var i = 0; i < labels.length; i++) {

			if ((labels[i].id.indexOf("mandatory") >= 0)) {
				var labelValue = labels[i].innerHTML;
				labels[i].innerHTML = "<b style='color:red'>*</b> \t"
						+ labelValue;
				labels[i].style.fontWeight = "bold";
			}
		}

	} else if (operation == 3) {

		var labels = document.getElementsByTagName("label");
		for ( var i = 0; i < labels.length; i++) {

			if ((labels[i].id != "")) {
				var labelValue = labels[i].innerHTML;
				var x = labelValue.split("\t");
				labelValue = x[x.length - 1];
				labels[i].innerHTML = labelValue;
				labels[i].style.fontWeight = "normal";
			}
		}

	} else {

		var labels = document.getElementsByTagName("label");
		for ( var i = 0; i < labels.length; i++) {

			if ((labels[i].id != "")) {
				var labelValue = labels[i].innerHTML;
				var x = labelValue.split("\t");
				labelValue = x[x.length - 1];
				labels[i].innerHTML = labelValue;
				labels[i].style.fontWeight = "normal";
			}
		}

		/*
		 * for ( var i = 0; i < inputs.length; i++) {
		 * 
		 * 
		 * if(inputs[i].id.indexOf("input")>=0) {
		 * inputs[i].style.backgroundImage = "none";
		 * inputs[i].style.backgroundColor = "#f1f1f1"; inputs[i].disabled =
		 * true; //document.forms[0]["form:name"].disabled = false } }
		 */
	}

}


function behaviorForm() {
	var selects = document.getElementsByTagName("select");

	loop: for ( var i = 0; i < selects.length; i++) {

		if (selects[i].id.indexOf("operations") >= 0) {

			var op = selects[i].value;

			setBehaviorForm(op);

			break loop;
		}

	}

}
