package br.com.pmm.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.security.core.context.SecurityContextHolder;

import br.com.pmm.primefaces.model.ChartModel;
import br.com.pmm.primefaces.model.DashboardModel;
import br.com.pmm.primefaces.model.DataTablePackagesModel;
import br.com.semavize.pmm.customer.model.Customer;
import br.com.test.mock.PackageMock;

@ManagedBean(name="customerDashboradBean")
@SessionScoped
public class CustomerDashboardBean implements Serializable {

	/**
	 * @author Marcos
	 * Classe bean que representa o Dashboard por Usuário (Customer)
	 */
	private static final long serialVersionUID = 1L;

	private ChartModel chartModel;
	private DashboardModel dashboardModel;
	private DataTablePackagesModel dataTablePkgModel;
	private Customer customer;
	
	@PersistenceContext
	EntityManager em;
	
	
	
	@PostConstruct
	public void init(){
		
		dashboardModel   = new DashboardModel();
		
		
		/*Iniciando os modelos necessários do dashboard*/
		chartModel 	     = new ChartModel();

		
		
		//Exemplo Mock (Passar aqui uma lista de pacotes)
		PackageMock pm = new PackageMock();
		dataTablePkgModel = new DataTablePackagesModel(pm.getPackageListExample());
		//
		
		
		customer = (Customer) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				
	}


	public DataTablePackagesModel getDataTablePkgModel() {
		return dataTablePkgModel;
	}

	public void setDataTablePkgModel(DataTablePackagesModel dataTablePkgModel) {
		this.dataTablePkgModel = dataTablePkgModel;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public ChartModel getChartModel() {
		return chartModel;
	}

	public void setChartModel(ChartModel chartModel) {
		this.chartModel = chartModel;
	}

	public DashboardModel getDashboardModel() {
		return dashboardModel;
	}

	public void setDashboardModel(DashboardModel dashboardModel) {
		this.dashboardModel = dashboardModel;
	}


		
}
