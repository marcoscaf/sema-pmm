package br.com.pmm.security.providers;

import java.util.ArrayList;
import java.util.Collection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.stereotype.Controller;

import br.com.semavize.pmm.customer.model.Customer;
import br.com.semavize.pmm.repository.CustomerRepository;

@SuppressWarnings("deprecation")
@Controller
@Scope("request")
public class CustomerAuthenticationProvider implements AuthenticationProvider {

	private CustomerRepository customerRepository;

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {

		try {
			//Lookup do repositório de clientes
			Context ctx = new InitialContext();
			customerRepository = (CustomerRepository) ctx
					.lookup("java:global/PMM-ear/PMM-ejb/CustomerRepositoryImpl");

		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Login e digitado na tela
		String login = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();

		// Obtebdo cliente por JPA
		Customer customer = customerRepository.findByLoginAndPassword(login,
				password);

		try {
			// Se o usuário não for null, eu o autentico no sistema
			if (customer != null) {
				Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				authorities.add(new GrantedAuthorityImpl(customer
						.getAuthority()));
				return new CustomerAuthenticationToken(customer,
						customer.getPassword(), authorities);

			} else {
				throw new AuthenticationServiceException(
						"Usuário não autenticado.");
			}

		} catch (AuthenticationServiceException e) {
			throw e;
		} catch (Throwable e) {
			throw new AuthenticationServiceException(
					"Ocorreu um erro no ato da autenticação.", e);
		}
	}

	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication)
				&& authentication
						.equals(UsernamePasswordAuthenticationToken.class);
	}

}
