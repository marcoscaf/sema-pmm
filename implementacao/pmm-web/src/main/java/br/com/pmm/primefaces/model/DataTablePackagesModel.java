package br.com.pmm.primefaces.model;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.SelectableDataModel;

import br.com.semavize.pmm.mailpackage.model.Package;

public class DataTablePackagesModel extends ListDataModel<Package> implements
		SelectableDataModel<Package> {
	
	private Package pkg;

	public DataTablePackagesModel(List<Package> packagelist) {
		super(packagelist);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Package getRowData(String rowKey) {

		List<Package> listMember = (List<Package>) getWrappedData();

		for (Package member : listMember) {
			if (member.toString().equals(rowKey))
				return member.clone();
		}
		return null;
	}

	@Override
	public Object getRowKey(Package pkg) {
		return pkg;
	}
	
	public void onRowSelect(SelectEvent event) {

		this.pkg = (Package) event.getObject();
		//TODO remover sysout
		System.out.println("Pacote com destinatário "+this.pkg.getNomeDestinatario()+" selecionado");
	}

	public Package getPkg() {
		return pkg;
	}

	public void setPkg(Package pkg) {
		this.pkg = pkg;
	}
	
	

}
