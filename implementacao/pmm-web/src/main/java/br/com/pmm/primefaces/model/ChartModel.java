package br.com.pmm.primefaces.model;

import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.stereotype.Service;


@Service
public class ChartModel {
    private BarChartModel barModel;
    private PieChartModel pieModel;
 
    public ChartModel() {
        createBarModel();
        createPieModel();
    }
 
    public BarChartModel getBarModel() {
        return barModel;
    }
     
    public PieChartModel getPieModel1() {
        return pieModel;
    }
      
    private void createBarModel() {
        barModel = initBarModel();
         
        barModel.setTitle("Bar Chart");
        barModel.setLegendPosition("ne");
         
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Gender");
         
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Births");
        yAxis.setMin(0);
        yAxis.setMax(200);
    }
     
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
 
        ChartSeries boys = new ChartSeries();
        boys.setLabel("Boys");
        boys.set("2004", 120);
        boys.set("2005", 100);
        boys.set("2006", 44);
        boys.set("2007", 150);
        boys.set("2008", 25);
 
        ChartSeries girls = new ChartSeries();
        girls.setLabel("Girls");
        girls.set("2004", 52);
        girls.set("2005", 60);
        girls.set("2006", 110);
        girls.set("2007", 135);
        girls.set("2008", 120);
 
        model.addSeries(boys);
        model.addSeries(girls);
         
        return model;
    }
 

     
    private void createPieModel() {
        pieModel = new PieChartModel();
         
        pieModel.set("A Caminho", 10);
        pieModel.set("Entregue", 20);
        pieModel.set("Atrasados", 15);
      

        pieModel.setShowDataLabels(true);
        pieModel.setTitle("Gráfico dos Pacotes");
        pieModel.setLegendPosition("w");
    }
 
    public void itemSelect(ItemSelectEvent event) {
    	
    	System.out.println("Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());
    	if(event.getItemIndex() == 0){
    		System.out.println("A Caminho");
    	}
    	else if(event.getItemIndex() == 1){
    		System.out.println("Entregue");
    	}
    	else{
    		System.out.println("Atrasados");
    	}

    }
}
