package br.com.pmm.primefaces.model;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;


/**
 * @author Marcos
 * Classe modelo para o Dashboard
 */

public class DashboardModel {
	private org.primefaces.model.DashboardModel  model;

	public DashboardModel() {
		model = new DefaultDashboardModel();
		
		//Criando colunas do dashboard
		DashboardColumn column1 = new DefaultDashboardColumn();
		DashboardColumn column2 = new DefaultDashboardColumn();
		DashboardColumn column3 = new DefaultDashboardColumn();

		/*
		 * Adicionando os Widgets nas colunas iniciaisO valor a ser passado no
		 * método addWidget deve ser o id do p:panel na pagina home
		 */
		column1.addWidget("graphic");
		column2.addWidget("packageList");
		column3.addWidget("schedule");

		//Adicionando as colunas no dashboard
		model.addColumn(column1);
		model.addColumn(column2);
		model.addColumn(column3);
		model.addColumn(new DefaultDashboardColumn());

	}

	/*Método que será acionado ao mudar um componente de lugar no dashboard*/
	public void handleReorder(DashboardReorderEvent event) {
		//TODO mudar implementação, colocar o que é necessário (salvar posição etc...)
		FacesMessage message = new FacesMessage();
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		message.setSummary("Reordered: " + event.getWidgetId());
		message.setDetail("Item index: " + event.getItemIndex()
				+ ", Column index: " + event.getColumnIndex()
				+ ", Sender index: " + event.getSenderColumnIndex());

		addMessage(message);
	}

	private void addMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public org.primefaces.model.DashboardModel getModel() {
		return model;
	}
}
