package br.com.semavize.pmm.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractRepository<T> implements Repository<T> {
	
	@PersistenceContext
	protected EntityManager em;

	private T entity;
	
	@SuppressWarnings("unchecked")
	@Override
	public T find(Long id) {
		return (T) em.find(entity.getClass(), id);
	}

	@Override
	public void remove(T entity) {
		em.remove(em.merge(entity));
	}

	@Override
	public void save(T entity) throws Exception {

		try {

			em.getTransaction().begin();

			em.persist(entity);

			em.getTransaction().commit();
			
		} catch (Exception e) {
			
			em.getTransaction().rollback();
			throw e;

		}

	}

	@Override
	public T update(T entity) {
		entity = em.merge(entity);
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByNamedQuery(String namedQuery) {
		return em.createQuery(namedQuery).getResultList();
	}

	public EntityManager getEm() {
		return em;
	}



}
