package br.com.semavize.pmm.repository.impl;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.semavize.pmm.customer.model.Customer;
import br.com.semavize.pmm.repository.AbstractRepository;
import br.com.semavize.pmm.repository.CustomerRepository;

@Local
@Stateless
public class CustomerRepositoryImpl extends AbstractRepository<Customer>
		implements CustomerRepository {

	@PersistenceContext
	EntityManager em;

	@Override
	public List<Customer> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Customer findByLoginAndPassword(String login, String password) {

		try {
			Customer customer = em
					.createNamedQuery("findCustomerByNameAndPassword",
							Customer.class).setParameter("login", login)
					.setParameter("password", password).getSingleResult();

			return customer;

		} catch (NoResultException e) {
			//Se não encontrar resultado a exceção NoResultException será lançada.
			return null;
		}

	}

}
