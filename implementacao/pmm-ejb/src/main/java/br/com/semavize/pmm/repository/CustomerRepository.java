package br.com.semavize.pmm.repository;

import br.com.semavize.pmm.customer.model.Customer;

public interface CustomerRepository {
	public Customer findByLoginAndPassword(String login, String password);
}
