package br.com.semavize.pmm.customer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMER")
@NamedQueries(@NamedQuery(name = "findCustomerByNameAndPassword", 
						  query = "SELECT c FROM Customer c where c.login= :login and c.password= :password"))
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_CUSTOMER")
	@SequenceGenerator(name = "SEQ_CUSTOMER", sequenceName = "SEQ_CUSTOMER",allocationSize=1)
	private Long id;

	/**
	 * Nom do usuario
	 */
	@Column(nullable = false, name = "name")
	private String name;
	
	/**
	 * Login do cliente para acessar o pmm.
	 */
	@Column(nullable = false, name = "login")
	private String login;
	
	/**
	 * Senha do cliente.
	 */
	@Column(nullable = false, name = "password")
	private String password;

	/**
	 * Usuário habilitado?
	 */
	@Column(nullable = false, name = "enable")
	private boolean enable;
	
	/**
	 * Permissão que o cliente terá.
	 */	
	@Column(nullable = false, name = "authority")
	private String authority;
	
	/**
	 * E-mail
	 */
	@Column(nullable = true, name = "email")
	private String email;

	/**
	 * Nome da empresa
	 */
	@Column(nullable = true, name = "company")
	private String company;

	/**
	 * Indica se o usuario está em fase de teste.
	 */
	@Column(nullable = false, name = "userTest")
	private Boolean userTest;

	/**
	 * Se o usuario está em fase de teste, uma quantidade de pacote é definido
	 * para ele testar.
	 */
	@Column(nullable = false, name = "qdtPackages")
	private Integer qdtPackages;

	public Customer() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Boolean getUserTest() {
		return userTest;
	}

	public void setUserTest(Boolean userTest) {
		this.userTest = userTest;
	}

	public Integer getQdtPackages() {
		return qdtPackages;
	}

	public void setQdtPackages(Integer qdtPackages) {
		this.qdtPackages = qdtPackages;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	
	@Override
	public String toString(){		
		return getName();
	}
	

}
