package br.com.semavize.pmm.repository;

import java.util.List;

/**
 * @author Marcos
 * 
 */

public interface Repository<T> {

	/**
	 * @param entity
	 */
	public void save(T entity) throws Exception;

	/**
	 * @param entity
	 * @return T
	 */
	public T update(T entity);

	/**
	 * @param entity
	 */
	public void remove(T entity);

	/**
	 * @return List<T>
	 */
	public List<T> getAll();

	/**
	 * @param id
	 * @return T
	 */
	public T find(Long id);

	/**
	 * 
	 * @param namedQuery
	 * @return List<T>
	 */
	public List<T> findByNamedQuery(String namedQuery);
}