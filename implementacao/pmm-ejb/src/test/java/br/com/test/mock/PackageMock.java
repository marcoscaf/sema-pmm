package br.com.test.mock;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDateTime;

import br.com.semavize.pmm.mailpackage.model.Package;


public class PackageMock {
	
	private List<Package> pkgList;
	
	
	public List<Package> getPackageListExample(){
		
		LocalDateTime time  = new LocalDateTime();
		pkgList = new ArrayList<Package>();
		
		pkgList.add(new Package(time, "Codigo 01", "BR00000000001", "100g", "13100231", "13044100", "Marcos Ferreira","Não entregue"));
		pkgList.add(new Package(time, "Codigo 02", "BR00000000002", "200g", "13100231", "13044100", "Sergio Augusto","Atrasado"));
		pkgList.add(new Package(time, "Codigo 03", "BR00000000003", "400g", "13100231", "13044100", "Sema","Encaminhado"));
		pkgList.add(new Package(time, "Codigo 04", "BR00000000004", "140g", "13100231", "13044100", "Teste 1","Não entregue"));
		pkgList.add(new Package(time, "Codigo 05", "BR00000000005", "121g", "13100231", "13044100", "CPqD","Atrasado"));
		pkgList.add(new Package(time, "Codigo 06", "BR00000000006", "43g", "13100231", "13044100", "ABC","Não entregue"));
		pkgList.add(new Package(time, "Codigo 07", "BR00000000007", "112g", "13100231", "13044100", "1234","Não entregue"));
		pkgList.add(new Package(time, "Codigo 08", "BR00000000008", "10g", "13100231", "13044100", "Marcos","Não entregue"));
		pkgList.add(new Package(time, "Codigo 09", "BR00000000009", "98g", "13100231", "13044100", "Ferreira","Entregue"));
		pkgList.add(new Package(time, "Codigo 10", "BR00000000010", "120g", "13100231", "13044100", "Cristiano","Entregue"));
		pkgList.add(new Package(time, "Codigo 11", "BR00000000011", "112g", "13100231", "13044100", "PMM","Não entregue"));
		pkgList.add(new Package(time, "Codigo 12", "BR00000000012", "134g", "13100231", "13044100", "11111111111","Não entregue"));
		pkgList.add(new Package(time, "Codigo 13", "BR00000000013", "177g", "13100231", "13044100", "Marcos Cristiano Alves Ferreira","Entregue"));
		
		
		return pkgList;
		
	}
	
}
