/* 
--------------- Drop Table, Drop Sequence and Drop index----------------------
DROP TABLE customer cascade;
DROP SEQUENCE seq_customer cascade; 
DROP INDEX idx_customer;
*/
   
/* --------------- Create Sequences --------------- */

  CREATE SEQUENCE seq_customer
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

  
/* --------------- Create Tables --------------- */

CREATE TABLE customer (
    id bigint NOT NULL,
    authority character varying(255) NOT NULL,
    company character varying(255),
    email character varying(255),
    enable boolean NOT NULL,
    login character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    qdtpackages integer NOT NULL,
    usertest boolean NOT NULL
);


/*---------------- Alter Owner ------------------*/  
ALTER TABLE seq_customer OWNER TO postgres;
ALTER TABLE public.customer OWNER TO postgres;


/*---------------- Create Index ------------------*/
CREATE UNIQUE INDEX idx_customer ON customer (ID);  

/*---------------- INSERTS ------------------*/
insert into Customer (id, authority, company, email, enable, login, name, password, qdtpackages, usertest) values (0,'ROLE_USER','Semaviz3','john.smith@mailinator.com',true,'marcos','John Smith','123','1000',false) 
  
